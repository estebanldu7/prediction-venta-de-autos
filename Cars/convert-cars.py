import pandas
import coremltools
from sklearn.linear_model import LinearRegression

rawdata = pandas.read_csv("cars.csv") #Read Csv

model = LinearRegression()
model.fit(rawdata[["modelo", "extras", "kilometraje", "estado"]], rawdata["precio"]) #training step 

coreml_model = coremltools.converters.sklearn.convert(model, ["modelo", "extras", "kilometraje", "estado"], "precio")

coreml_model.author = "Esteban Preciado"
coreml_model.license = "CCO"
coreml_model.description = "This model predicst the car sale value with some parameters."

coreml_model.save("Cars.mlmodel")

