//
//  ViewController.swift
//  CarML
//
//  Created by Esteban Preciado on 10/21/18.
//  Copyright © 2018 Esteban Preciado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var modelSegmentedControl: UISegmentedControl!
    @IBOutlet weak var extraSwitch: UISwitch!
    @IBOutlet weak var kmsLabel: UILabel!
    @IBOutlet weak var kmsSlider: UISlider!
    @IBOutlet weak var statusSegmentedControl: UISegmentedControl!
    @IBOutlet weak var priceLabel: UILabel!
    
    let cars = Cars()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.stackView.setCustomSpacing(25, after: self.modelSegmentedControl)
        self.stackView.setCustomSpacing(25, after: self.extraSwitch)
        self.stackView.setCustomSpacing(60, after: self.statusSegmentedControl)
        
        self.calculateValue()
    }
    
    @IBAction func calculateValue() {
        
        //Format the slider
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        let formattedKms = formatter.string(for: self.kmsSlider.value) ?? "0"
        self.kmsLabel.text = "Kilometraje: \(formattedKms) kms"

        //Calculate price throught ML
        if let prediction = try? cars.prediction(
            modelo: Double(self.modelSegmentedControl.selectedSegmentIndex),
            extras: self.extraSwitch.isOn ? Double(1.0) : Double(0.0),
            kilometraje: Double(self.kmsSlider.value),
            estado: Double(self.statusSegmentedControl.selectedSegmentIndex)){
            
            let clampValue = max(500, prediction.precio) // Get max prince between two values
            
            formatter.numberStyle = .currency
            
            self.priceLabel.text = formatter.string(for: clampValue)
            
        } else {
            self.priceLabel.text = "Error"
        }

    }
}
